package Interface;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Stroke;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Graphics2D;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class CluedoCanvas extends JPanel {
	
	private final int sqSize = 28;
	private final int numCols = 25;
	private final int numRows = 25;
	private final int left = 25;
	private final int top = 20;
	private Point selected;
	protected Point cursorLocation;
	private final char[][] map = {
			{'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X',  67, 'X', 'X', 'X', 'X',  67, 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X'},   //0
			{  2,   2,   2,   2,   2,   4, 'X',  67,  67,  67,   3,   2,  2,    4,  67,  67,  67, 'X',   3,   2,   2,   2,   2,   2,   2},   //1
			{'K', 'K', 'K', 'K', 'K',   5,  67,  67,   3,   2, 'B', 'B', 'B', 'B',   2,   4,  67,  67,   1, 'G', 'G', 'G', 'G', 'G', 'G'},   //2
			{'K', 'K', 'K', 'K', 'K',   5,  67,  67,   1, 'B', 'B', 'B', 'B', 'B', 'B',   5,  67,  67,   1, 'G', 'G', 'G', 'G', 'G', 'G'},   //3
			{'K', 'K', 'K', 'K', 'K',   5,  67,  67,   1, 'B', 'B', 'B', 'B', 'B', 'B',   5,  67,  67,   1, 'G', 'G', 'G', 'G', 'G', 'G'},   //4
			{  7, 'K', 'K', 'K', 'K',   5,  67,  67, 'B', 'B', 'B', 'B', 'B', 'B', 'B', 'B',  67,  67,  67,   0,   7,   7,   7,   7, 'X'},   //5
			{'X',   0,   7,   7, 'R',   6,  67,  67,   1, 'B', 'B', 'B', 'B', 'B', 'B',   5,  67,  67,  67,  67,  67,  67,  67,  67,  67},   //6
			{ 67,  67,  67,  67,  67,  67,  67,  67,   0, 'B',   7,   7,   7,   7, 'B',   6,  67,  67,  67,  67,  67,  67,  67,  67, 'X'},   //7
			{'X',  67,  67,  67,  67,  67,  67,  67,  67,  67,  67,  67,  67,  67,  67,  67,  67,  67,   3,   2,   2,   2,   2,   2,   2},	//8
			{  3,   2,   2,   2,   4,  67,  67,  67,  67,  67,  67,  67,  67,  67,  67,  67,  67,  67, 'P', 'P', 'P', 'P', 'P', 'P', 'P'},	//9
			{  1, 'D', 'D', 'D', 'D',   2,   2,   4,  67,  67, 'Z', 'Z', 'Z', 'Z', 'Z',  67,  67,  67,   1, 'P', 'P', 'P', 'P', 'P', 'P'},	//10
			{  1, 'D', 'D', 'D', 'D', 'D', 'D',   5,  67,  67, 'Z', 'Z', 'Z', 'Z', 'Z',  67,  67,  67,   1, 'P', 'P', 'P', 'P', 'P', 'P'},	//11
			{  1, 'D', 'D', 'D', 'D', 'D', 'D', 'D',  67,  67, 'Z', 'Z', 'Z', 'Z', 'Z',  67,  67,  67,   0,   7,   7,   7, 'P',   7,   7},	//12
			{  1, 'D', 'D', 'D', 'D', 'D', 'D',   5,  67,  67, 'Z', 'Z', 'Z', 'Z', 'Z',  67,  67,  67,  67,  67,  67,  67,  67,  67, 'X'},	//13
			{  1, 'D', 'D', 'D', 'D', 'D', 'D',   5,  67,  67, 'Z', 'Z', 'Z', 'Z', 'Z',  67,  67,  67,   3,   2, 'L',   2,   2,   4, 'X'},	//14
			{  0,   7,   7,   7,   7,   7, 'D',   6,  67,  67, 'Z', 'Z', 'Z', 'Z', 'Z',  67,  67,   3, 'L', 'L', 'L', 'L', 'L', 'L',   2},   //15
			{'X',  67,  67,  67,  67,  67,  67,  67,  67,  67, 'Z', 'Z', 'Z', 'Z', 'Z',  67,  67, 'L', 'L', 'L', 'L', 'L', 'L', 'L', 'L'},   //16
			{ 67,  67,  67,  67,  67,  67,  67,  67,  67,  67,  67,  67,  67,  67,  67,  67,  67,   0, 'L', 'L', 'L', 'L', 'L', 'L', 'L'},	//17
			{'X',  67,  67,  67,  67,  67,  67,  67,  67,   3,   2, 'H', 'H',   2,   4,  67,  67,  67,   0,   7,   7,   7,   7,   7,   7},	//18
			{  2,   2,  2,    2,   2,   2,   5,  67,  67,   1, 'H', 'H', 'H', 'H',   5,  67,  67,  67,  67,  67,  67,  67,  67,  67,  67},	//19
			{'O', 'O', 'O', 'O', 'O', 'O',   5,  67,  67,   1, 'H', 'H', 'H', 'H',   5,  67,  67,  67,  67,  67,  67,  67,  67,  67, 'X'},	//20
			{'O', 'O', 'O', 'O', 'O', 'O',   5,  67,  67,   1, 'H', 'H', 'H', 'H',   5,  67,  67,   1,   2,   2,   2,   2,   2,   2,   2},	//21
			{'O', 'O', 'O', 'O', 'O', 'O',   5,  67,  67,   1, 'H', 'H', 'H', 'H',   5,  67,  67,   1, 'S', 'S', 'S', 'S', 'S', 'S', 'S'},	//22
			{'O', 'O', 'O', 'O', 'O', 'O',   6,  67,  67,   1, 'H', 'H', 'H', 'H',   5,  67,  67,   0, 'S', 'S', 'S', 'S', 'S', 'S', 'S'},	//23
			{'O', 'O', 'O', 'O', 'O',   6, 'X',  67, 'X',   1, 'H', 'H', 'H', 'H',   5, 'X',  67, 'X',   1, 'S', 'S', 'S', 'S', 'S', 'S'}};	//24


	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
	}
	
	public void paint(Graphics g) {
		g.setFont(new Font("Arial", Font.PLAIN, 9));
		Image grass = (new ImageIcon("images/grass.png")).getImage();
		
		for (int row = 0; row < numRows; row++){
			// print the index number for board
			String index = Integer.toString(row);
			int x = g.getFontMetrics().stringWidth(index);
			if (x == 5) x += 10;        // to align the index number
			g.setColor(Color.BLACK);
			g.drawString(index, x-2, top+18+row*sqSize);
			
			for (int col = 0; col < numCols; col++){
				((Graphics2D) g).setStroke(new BasicStroke(1));
				if (map[row][col] == 67){
					g.setColor(Color.orange);
					g.fillRect(left+col*sqSize, top+row*sqSize, sqSize, sqSize);
					g.setColor(Color.BLACK);
					g.drawRect(left+col*sqSize, top+row*sqSize, sqSize, sqSize);
				} else if (map[row][col] == 'Z'){
					g.setColor(Color.YELLOW);
					g.fillRect(left+col*sqSize, top+row*sqSize, sqSize, sqSize);
				} else if (map[row][col] == 'X'){
					g.drawImage(grass, left+col*sqSize, top+row*sqSize, this);
				} else{
					g.setColor(new Color(173, 163, 130));
					g.fillRect(left+col*sqSize, top+row*sqSize, sqSize, sqSize);
				}
				
				// draw border outlines for ROOM
				g.setColor(new Color(146, 27, 21));
				((Graphics2D) g).setStroke(new BasicStroke(5));
				if (map[row][col] == 0 || map[row][col] == 1 || map[row][col] == 3)
					g.drawLine(left+col*sqSize+2, top+row*sqSize, left+col*sqSize+2, top+(row+1)*sqSize);              // left side border
				if (map[row][col] == 0 || map[row][col] == 6 || map[row][col] == 7)
					g.drawLine(left+col*sqSize+2, top+(row+1)*sqSize-2, left+(col+1)*sqSize-2, top+(row+1)*sqSize-2);  // bottom side border
				if (map[row][col] == 2 || map[row][col] == 3 || map[row][col] == 4)
					g.drawLine(left+col*sqSize+2, top+row*sqSize, left+(col+1)*sqSize-2, top+row*sqSize);             // top border
				if (map[row][col] == 4 || map[row][col] == 6 || map[row][col] == 5)
					g.drawLine(left+(col+1)*sqSize-3, top+row*sqSize, left+(col+1)*sqSize-3, top+(row+1)*sqSize);     // right side border
			}
		}
		
		// draw the secret passages and logo
		Image secret1 = (new ImageIcon("images/secret.png")).getImage();
		Image secret2 = (new ImageIcon("images/secretKitchen.png")).getImage();
		Image secret3 = (new ImageIcon("images/secretConservatory.png")).getImage();
		Image secret4 = (new ImageIcon("images/secretStudy.png")).getImage();
		Image logo = (new ImageIcon("images/logo.png")).getImage();
		g.drawImage(secret1, left, top+24*sqSize, this);
		g.drawImage(secret2, left, top+sqSize, this);
		g.drawImage(secret3, left+sqSize*24, top+sqSize, this);
		g.drawImage(secret4, left+sqSize*24, top+24*sqSize, this);
		g.drawImage(logo, left+sqSize*10, top+10*sqSize, this);
		
		// print the index number for the board
		for (int col = 0; col < numCols; col++){
			String index = Integer.toString(col);
			int x = g.getFontMetrics().stringWidth(index);
			if (x == 5) x += 10;        // to align the index number
			g.setColor(Color.BLACK);
			g.drawString(index, left+col*sqSize+x, 10);
			g.drawString(index, left+col*sqSize+x, top+18+25*sqSize);
			g.drawString(index, left+25*sqSize+x, top+col*sqSize+18);
		}
		
		// draw the black outline on the board
		g.setColor(Color.BLACK);
		((Graphics2D) g).setStroke(new BasicStroke(8));
		g.drawRect(left, top, 700, 700);
	}
	
	public static void main(String [] args){
		CluedoFrame frame = new CluedoFrame();
	}
}
